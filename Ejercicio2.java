package visualTests;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

/**
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio2 extends Browsers{
	// Define screen for sikuli.
	Screen screen;
	
	// Initialize 
	@BeforeClass
	public void init() {
		screen = new Screen();
		//Settings.MinSimilarity = 0.95;
	}
	
	
	@Test
	public void f() {
		try {
			driver.get("http://compras135.ufm.edu/");
			//encontrar el boton ingresar
			Region region = screen.wait("img/images/ingresar.png").highlight();
			region.click();
			region.click();
			
			//Email
			region = screen.wait("img/images/emailLogin.png");
			region.click();
			region.type("xik");
			region = screen.wait("img/images/nextEmail.png");
			region.click();
			
			//Password
			region = screen.wait("img/images/passLoginSc.png");
			region.type("xik$2015");
			region = screen.wait("img/images/nextEmail.png");
			region.click();
			ReadUrlFile.Wait(8000);
			
			//----------------------------------------------------------
			
			region = screen.wait("img/images/UFMapps.png");
			region.click();
			region = screen.wait("img/images/AA.png");
			region.click();
			ReadUrlFile.Wait(1000);
			region = screen.wait("img/images/CA.png");
			region.click();
			ReadUrlFile.Wait(1000);
			region = screen.wait("img/images/AdmLogos.png");
			region = screen.wait("img/images/AdmLogos.png");
			region.click();
			ReadUrlFile.Wait(2000);
			region = screen.wait("img/images/NuevaSolicitud.png");
			region.click();
			ReadUrlFile.Wait(2000);
			region = screen.wait("img/images/NombreF.png");
			region.click();
			region.type("Karl Heinz");
			region = screen.wait("img/images/SelectName.png");
			region.click();
			region = screen.wait("img/images/categoria.png");
			region.click();
			region = screen.wait("img/images/catedratico.png");
			region.click();
			region = screen.wait("img/images/Titulo.png");
			region.click();
			region = screen.wait("img/images/NuevoLogo.png");
			region.click();
			region = screen.wait("img/images/titdes.png");
			region.click();
			region = region.wait("img/images/descTitulo.png");
			region.click();
			region.type("TestXIK_MP");
			region =screen.wait("img/images/CantLogos.png");
			region.click();
			region.type("5");
			ReadUrlFile.Wait(2000);
			region =screen.wait("img/images/Ciclo.png");
			region.click();
			region =screen.wait("img/images/MEECON.png");
			region.click();
			ReadUrlFile.Wait(2000);
			region =screen.wait("img/images/Cupo.png");
			region.click();
			region.type("16");
			ReadUrlFile.Wait(2000);
			region =screen.wait("img/images/Secc.png");
			region.click();
			region.type("A");
			region =screen.wait("img/images/EnviarSolicitud.png");
			region.click();
			ReadUrlFile.Wait(2000);
			region = screen.wait("img/images/CA.png");
			region.click();
			ReadUrlFile.Wait(1000);
			region = screen.wait("img/images/AdmLogos.png");
			region = screen.wait("img/images/AdmLogos.png");
			region.click();
			ReadUrlFile.Wait(2000);
			region = screen.wait("img/images/titulox2.png");
			region.click();
			region.type("TestXIK_MP");
			region = screen.wait("img/images/Buscar.png");
			region.click();
			
			ReadUrlFile.Wait(5000);
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}	
